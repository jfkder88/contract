pragma solidity ^0.4.24;

import "./SafeMath.sol";
import "./Ownable.sol";
import "./Erc20.sol";

/**
 * @title Arbitrage
 * @dev This contract can be used when payments need to be received by a group
 * of people and split proportionately to some number of shares they own.
 */
contract  Arbitrage is StandardToken, Ownable {
  using SafeMath for uint256;

  
  string public constant name = 'IFT TOKEN'; 
  string public constant symbol = 'IFT'; 
  
  uint8 private constant PREVOTING = 0;
  uint8 private constant OPENVOTING = 1;
  uint8 private constant VOTING = 2;
  uint8 private constant FAILEDVOTING = 3;
  uint8 private constant OKVOTING = 4; 
  
  uint8 public constant NOTVOTED = 1;
  uint8 public constant VOTEDYES = 3;
  uint8 public constant VOTEDNO = 2;
  
  uint256 public constant PERIOD_PREVOTING = 182 * 24 * 60 * 60; //182 days ( half a year)
  uint256 public constant PERIOD_VOTING = 72 * 60 * 60; //72 hours
  uint256 public constant PERIOD_FAILEDVOTING = 30 * 24 * 60 * 60; //30 days
  uint256 public constant PERIOD_AFTERVOTING = 30 * 24 * 60 * 60; //30 days
  uint256 public constant PERIOD_HARDCAP = 182 * 24 * 60 * 60; //182 days
  uint256 public constant VOTELEVEL = 5;
  
  uint256 public totalSupply = 0;
  
  bool public halted  = false;
  

  uint256 private _totalPayment = 0;
  uint256 private _fund = 0;
  uint256 private _paypertoken = 0;
  
  uint256 private _totalYes = 0;
  uint256 private _totalNo = 0;
  uint256 private _voteGen = 1;
  
  uint256 public totalHolders = 0;
  
  //mapping(address => uint256) private _tokens;
  mapping(address => uint256) private _payed;
  mapping(address => uint8) private _voted;
  mapping(address => uint256) private _votedIn;
  
  address[] private _arbiters;

  uint256 public voteInitTimestamp;
  uint256 public voteResultTimestamp;
  uint256 public startTimestamp; 
  uint256 public currentState;
  
  /**
   * @dev Constructor
   */
  //constructor(address[] payees, uint256[] shares) public payable {
  constructor() public payable {

    currentState = PREVOTING;
    startTimestamp = now;
    
  }
  

  /**
   * @dev payable fallback
   */
  function () external payable {
      require(msg.data.length == 0);
      _fund = _fund.add(msg.value);
      if( totalSupply != 0)
            recalcPayment();
  }



   /**
    * suspend functionality
    */
  
  event Halt();
  
  modifier whenNotHalted {
    require(!halted);
    _;
  }
  
  function halt() public onlyOwner {
      require ( totalSupply == 0);

      halted = true;
      emit Halt();
  }
  
  /**
   *    VIEW block 
   */
   
  /**
   * @return the total shares of the contract.
   */
  function totalTokens() public view returns(uint256) {
    return totalSupply;
  }
  /**
   * @return the total funds on the contract.
   */
  function getFund() public view returns (uint256) {
      return _fund;
  }
  
  /**
   * @return amount distributed.
   */
  function totalPayment() public view returns(uint256) {
    return _totalPayment;
  }

  /**
   * @return the shares of an account.
   */
  function balanceOf(address account) public view returns(uint256) {
    return balances[account];
  }

  /**
   * @return the amount already released to an account.
   */
  function paymentByAddress(address account) public view returns(uint256) {
    return _payed[account];
  }

  /**
   * @return the address of a payee.
   */
  function arbitrAddress(uint256 index) public view returns(address) {
    return _arbiters[index];
  }
  
  /**
   * @return current price of one token
   */
  function getSharePrice() public view returns(uint256) {
    return _paypertoken;
  }
  
  /**
   * @return current vote generation.
   */
  function getVoteGen() public view returns (uint256) {
      return _voteGen;
  }

  /**
   *   VOTING 
   */
   
  /**
   * @dev initiate voting
   */ 
  function initiateVoting() whenNotHalted public {
      require ( _voted[msg.sender] > 0 );
      require(balances[msg.sender] > VOTELEVEL);
      require(currentState == PREVOTING || currentState == FAILEDVOTING);
      if (currentState == PREVOTING && now > (startTimestamp + PERIOD_PREVOTING)){
          currentState = VOTING;
          voteInitTimestamp = now;
      }
      if (currentState == FAILEDVOTING && now > (voteResultTimestamp + PERIOD_FAILEDVOTING )){
          currentState = VOTING;
          voteInitTimestamp = now;
      }
  }
  
  
  /**
   * @dev vote
   * @param voice 2: no 3: yes
   */
  function vote(uint8 voice) whenNotHalted public {
      require (_votedIn[msg.sender] < _voteGen);
      require( balances[msg.sender] > 0);
      require (currentState == VOTING );
      require ( voice == 2 || voice == 3);
      require ( now < ( voteInitTimestamp + PERIOD_VOTING) );
      _voted[msg.sender] = voice;
      _votedIn[msg.sender] = _voteGen;
      if (voice == 3)
        _totalYes +=1;
      else
        _totalNo +=1;
  }
  
  function getCurrentVoteResult() whenNotHalted public view returns (bool) {
      
      require (_totalYes >0 || _totalNo >0);
      require ( (_totalYes + _totalNo) > totalHolders.div(2) );
      if ( _totalYes > _totalNo)
        return true;
      else
        return false;
  }
  
  function endVote() whenNotHalted public {
      require ( currentState == VOTING);
      require ( now > (voteInitTimestamp + PERIOD_VOTING) );
      if (getCurrentVoteResult())
        currentState = OKVOTING;
      else
        currentState = FAILEDVOTING;
      voteResultTimestamp = now;
      _totalNo = 0;
      _totalYes = 0;
      _voteGen = _voteGen + 1;
      /*
      for (uint i = 0;i<totalHolders; i++) {
          _voted[_arbiters[i]] = NOTVOTED;
      }
      */
      
  }
  
  
  
 
  /**
   * @dev process payment for given account
   * 
   * @param account address to send payment
   */
  function makepayment(address account) whenNotHalted  public onlyOwner {
    require(balances[account] > 0);
    require( currentState == OKVOTING);
    require ( now > voteResultTimestamp + PERIOD_HARDCAP);
    /*
        
    
    uint256 totalReceived = address(this).balance.add(_totalReleased);
    
    uint256 payment = totalReceived.mul(
      _shares[account]).div(
        _totalShares).sub(
          _released[account]
    );
    */
    uint256 payment = _paypertoken.mul(balances[account]);
    require(payment != 0);

    _payed[account] = _payed[account].add(payment);
    _totalPayment = _totalPayment.add(payment);
    _fund = _fund.sub(payment);
    totalSupply = totalSupply.sub(balances[account]);
    balances[account] = 0;
    account.transfer(payment);
  }


   /**
   * @dev Add a new arbiter to the contract.
   * @param account The address of the arbiter to add.
   * @param tokens The number of tokens owned by the arbiter.
   */
  function addArbiter(address account,uint256 tokens) whenNotHalted public onlyOwner returns (uint256) {
      require(currentState != VOTING);
      require(currentState != OKVOTING);
      require(account != owner);   //restrict owner from receiving tokens
      require(_voted[account] == 0);  //check if arbiter already in list
      
      _arbiters.push (account);   // add arbiter to global list
      totalHolders += 1;
      balances[account] = 0;  // 
      _voted[account] = NOTVOTED;
      _votedIn[account] = 0;
      if(tokens > 0)
          issueTokens(account,tokens);
      return totalSupply;
  }
  
   /**
   * @dev Add tokens to specific arbiter.
   * @param account The address of the arbiter.
   * @param tokens The number of tokens to add.
   */
  function issueTokens(address account, uint256 tokens) whenNotHalted public onlyOwner returns (uint256) {
      require(currentState != VOTING);
      require(account != address(0));
      require(_voted[account] != 0);
      
      balances[account] = balances[account].add(tokens);
      totalSupply = totalSupply.add(tokens);
      recalcPayment();
      return balances[account];
      
  }
  
  function recalcPayment() internal {
      require( totalSupply != 0);
      _paypertoken = _fund.div(totalSupply);
  }
  
  
  
  
  
  /**
   * @dev Transfer token for a specified address when not paused.
   * we honor erc20 function convention, but only contract itself may be the 
   * the recipient. and with this it will make a payment
   * @param _to The address to transfer to
   * @param _value The amount to be transferred.
   */
  function transfer(address _to, uint256 _value) whenNotHalted public returns (bool)  {
    require(_to == address(this));    //make sure holders can't transfer to anyone except contract
    require(currentState == OKVOTING && now > (voteResultTimestamp + PERIOD_AFTERVOTING)); // make sure we are in voted state
    require(balances[msg.sender] >= _value);
    uint256 payment = _paypertoken.mul(_value);
    
    if (payment >0){
        balances[msg.sender] = balances[msg.sender].sub(_value);
        totalSupply = totalSupply.sub(_value);
        _fund = _fund.sub(payment);
        _payed[msg.sender] = _payed[msg.sender].add(payment);
        _totalPayment = _totalPayment.add(payment);
       address acc = msg.sender;
       acc.transfer(payment);
    }
    return true;
    
  }
  
  /**
   * DEBUG
   */
  /*
  function burn(address account, uint256 _value) public onlyOwner returns (bool success) {
        require(balances[account] >= _value);   // Check if the sender has enough
        balances[account] -= _value;            // Subtract from the sender
        totalSupply -= _value;                      // Updates totalSupply
        emit Burn(msg.sender, _value);
        return true;
    }
    
  function _dsetCurrentState(uint256 state) public onlyOwner {
      currentState = state;
  } 
  function _dsetVoteInitTimestamp(uint256 period) public onlyOwner {
      voteInitTimestamp = now - period;
  }
  
  function _dsetVoteResultTimestamp(uint256 period) public onlyOwner {
      voteResultTimestamp = now - period;
  }
  
  function _dsetStartTimestamp(uint256 period) public onlyOwner {
      startTimestamp = now - period;
  }
 
  */
  
}
